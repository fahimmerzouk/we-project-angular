import {Adress} from '../domain/Adress';
import {Sport} from '../domain/Sport';
export class Person {
    
    name: string = '';
    email:string ='';
    password:string ='';
    firstname: string = '';
    id: number ;
    adresse: Adress ;
    sports: Sport[];
}