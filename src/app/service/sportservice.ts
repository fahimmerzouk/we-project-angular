import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Sport } from '../domain/Sport';

@Injectable()
export class sportservice {

    constructor(private http: HttpClient) {}

    getAllSports() {
        return this.http.get<Sport[]>('/api/SportService/getAll')
                    .toPromise()
                    .then(res => <Sport[]> res)
                    .then(data => { return data; });
    }
}