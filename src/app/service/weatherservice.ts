import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Person } from '../domain/Person';
import { Weather } from '../domain/Weather';

@Injectable()
export class weatherservice {

    constructor(private http: HttpClient) {}

    getWeather(currentUser) {
        return this.http.get<Weather>('/api/WeatherService', {
            params: {
              _city: currentUser.adresse.adresse
            }
          })
                    .toPromise()
                    .then(res => <Weather> res)
                    .then(data => { return data; });
    }
}