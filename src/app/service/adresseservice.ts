import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Adress } from '../domain/Adress';

@Injectable()
export class adresseservice {

    constructor(private http: HttpClient) {}

    getAllAdresses() {
        return this.http.get<Adress[]>('/api/AdressService/getAll')
                    .toPromise()
                    .then(res => <Adress[]> res)
                    .then(data => { return data; });
    }
}