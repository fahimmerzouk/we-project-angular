import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Person } from '../domain/Person';

@Injectable()
export class personservice {

    constructor(private http: HttpClient) {}

    getAllPersons() {
        return this.http.get<Person[]>('/api/PersonService/getAll')
                    .toPromise()
                    .then(res => <Person[]> res)
                    .then(data => { return data; });
    }
    findByEmail(email){
            return this.http.get<Person>('/api/PersonService/email', {
              params: {
                _email: email
              }
            }).toPromise()
            .then(res => <Person> res)
            .then(data => {return data;})
    }

    findById(id){
      
      return this.http.get<Person>('/api/PersonService', {
        params: {
          _id: id
        }
      }).toPromise()
      .then(res => <Person> res)
      .then(data => {return data;})
    }

                
        
    
}