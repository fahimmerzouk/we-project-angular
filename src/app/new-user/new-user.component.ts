import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import {Sport} from '../domain/Sport';
import {Adress} from '../domain/Adress';
import {sportservice} from '../service/sportservice';
import {adresseservice} from '../service/adresseservice';
import {personservice} from '../service/personservice';
import { Person } from '../domain/Person';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css'],
  providers:[sportservice,adresseservice,personservice]
})
export class NewUserComponent implements OnInit {

  constructor( private http: HttpClient,
              private router: Router ,
              private sportServive: sportservice ,
              private personService: personservice ,
              private adresseService: adresseservice ) { }
  firstname:string = '';
  name:string = '';
  email:string = '';
  password:string = '';
  selectedSports= [];
  selectedAdress=[];

  sports: Sport[] = [];
  sportsSettings= {};
  adresses :Adress[]=[];
  adresseSettings= {};

  cuurentUser :Person;
  ngOnInit() {
    this.sportServive.getAllSports().then(sports => this.sports = sports);
    this.adresseService.getAllAdresses().then(adresses => this.adresses = adresses);

    this.sportsSettings = { 
      singleSelection: false,
      idField: 'id',
      textField: 'nomSport',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 10,
      allowSearchFilter: true
    };
    this.adresseSettings = { 
      singleSelection: true,
      idField: 'id',
      textField: 'adresse',
      itemsShowLimit: 1,
      allowSearchFilter: true
    };
  }
  addUser(){
    console.log("name",this.name);
    console.log("firstname",this.firstname);
    console.log("email",this.email);
    console.log("password",this.password);
    console.log("adress",this.selectedAdress);
    console.log("sports",this.selectedSports);

    return this.http.post<Person>("/api/PersonService/add",
    {
      "email":this.email,
      "name": this.name,
      "firstname": this.firstname,
      "password": this.password,
      "adresse": this.selectedAdress[0],
      "sports":this.selectedSports
    })
    .subscribe(
        (val) => {
          this.cuurentUser = val;
          console.log("POST call successful value returned in body", val);
          this.router.navigate(['/dashboard',this.cuurentUser["id"]]);
        },
        response => {
            console.log("POST call in error", response);
        },
        () => {
            console.log("The POST observable is now completed.");
        });
        
  }
    
}

