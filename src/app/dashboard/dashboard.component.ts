import { Component, OnInit } from '@angular/core';
import { Person } from '../domain/Person';
import { Sport } from '../domain/Sport';
import { ActivatedRoute } from '@angular/router';
import {personservice} from '../service/personservice';
import {weatherservice} from '../service/weatherservice';
import { Weather } from '../domain/Weather';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers:[personservice,weatherservice]
})
export class DashboardComponent implements OnInit {

  
  constructor(private route : ActivatedRoute,
              private personService : personservice,
              private weatherService : weatherservice) { }
  id : number;
  currentUser : Person;
  weather : Weather
  sportsProposes : Sport[] = [];

  ngOnInit() {
    this.id=parseInt(this.route.snapshot.paramMap.get("id"));
    console.log("id dans dashboard : ",this.id);
    this.personService.findById(this.id).then(u => this.currentUser = u)
    .then(data=>{
      this.weatherService.getWeather(this.currentUser).then(w=>{
        this.weather =w;
        console.log("weather : ", this.weather);
        console.log("currentUser",this.currentUser);
        this.proposerSport();
        console.log("sports proposes ",this.sportsProposes);
      })
    }
      );
  }

  proposerSport(){
    this.currentUser.sports.map((sport) => {
      if(sport.couvert == 0 && this.weather.main != "Rain" && this.weather.main != "Snow"){
        this.sportsProposes.push(sport);
      }
      else if(sport.couvert == 1){
        this.sportsProposes.push(sport);
      }
    })
  
    return this.sportsProposes;
  }
   
  }

