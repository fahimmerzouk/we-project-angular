import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {personservice} from '../service/personservice';
import { Person } from '../domain/Person';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers:[personservice]
})
export class LoginComponent implements OnInit {

  constructor(private router: Router ,
    private personService: personservice  ) { }

  email:string = '' ;
  password:string ='';

  user: Person;

  ngOnInit() {
  }
  
  signup= function () {
    this.router.navigate(['/new-user']);
  };
  signin= function () {
    console.log("email parametre : ",this.email);
    this.personService.findByEmail(this.email).then(u => this.user = u)
      .then(data => {
        console.log("this.user dans signin" , this.user);
      if(this.user == null){this.router.navigate(['/login']);}
      else if(this.user["email"] == this.email && this.user["password"] == this.password){
          console.log("bien authentifié")
          console.log("this.user   : ",this.user);
          this.router.navigate(['/dashboard',this.user["id"]]);
      }else{
        console.log("mal authentifié")
        console.log("this.user   : ",this.user);
        console.log("this.user[email]",this.user["email"]);
        console.log("this.email",this.email);
        console.log("this.user[password]",this.user["password"]);
        console.log("this.password",this.password);
        this.router.navigate(['/login']);
      };
      });
  };

}
